package br.ufrn.imd.smartparking.utils.validators;

/**
 * Check if a name input is valid.
 *
 * @author Rubem Kalebe
 * @version 10.21.2016
 */

public class CheckName {

    /**
     * Check if a name input is valid.
     * For now, we just verify the string length.
     * @param name User full name
     * @return True if date length is greater than 0 and False, otherwise
     */
    public static boolean isValid(String name) {
        return name.length() > 0;
    }

}