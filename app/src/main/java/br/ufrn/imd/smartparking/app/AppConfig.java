package br.ufrn.imd.smartparking.app;

/**
 * Class for app constants.
 *
 * @author Rubem Kalebe
 * @version 07.03.2017
 */

public class AppConfig {

    // Constant to work with QR codes, especially data transfer
    public static final String QR_CODE = "qr_code";

    public static final String REQUEST_LOGIN = "request_login";
    public static final String REQUEST_SIGNUP = "request_signup";
    public static final String REQUEST_DRIVER = "request_driver";
    public static final String REQUEST_ENTRY = "request_entry";
    public static final String REQUEST_PARK = "request_park";
    public static final String REQUEST_EXIT = "request_exit";

    public static final String URL_LOGIN = "http://177.135.36.71:8080/SmartParkingWS/login";
    public static final String URL_REGISTER = "http://177.135.36.71:8080/SmartParkingWS/createDriver";
    public static final String URL_GET_DRIVER = "http://177.135.36.71:8080/SmartParkingWS/getDriver";
    public static final String URL_ENTRY = "http://45.55.40.43:9000/entry";
    public static final String URL_PARK = "http://177.135.36.71:8080/SmartParkingWS/park";
    public static final String URL_EXIT = "http://45.55.40.43:9000/exit";

    public static final String ERROR_CODE_KEY = "error_code";

    public static final String DRIVER_KEY = "driver";
    public static final String FULL_NAME_KEY = "full_name";
    public static final String BIRTHDATE_KEY = "birthdate";
    public static final String PHONE_NUMBER_KEY = "phone_number";
    public static final String EMAIL_KEY = "email";
    public static final String GOOGLE_ID_KEY = "googleID";
    public static final String ID_DOCUMENT_KEY = "id_document";
    public static final String PASSWORD_KEY = "password";
    public static final String HAS_DISABILITY_KEY = "has_disability";
    public static final String SIGN_IN_WITH_GOOGLE = "signInWithGoogleBool";

    public static final String ID_KEY = "id";
    public static final String SPOT_ID_KEY = "spot_id";
    public static final String DRIVER_ID_KEY = "driver_id";

    public static final int DRIVER_RETRIEVE_SUCCESS = 0;
    public static final int DRIVER_NOT_FOUND = 1;
    public static final int DRIVER_WRONG_PASS = 3;

    public static final int DRIVER_INSERT_SUCCESS = 0;
    public static final int DRIVER_INSERT_ERROR = 1;
    public static final int DRIVER_INSERT_DUPLICATE = 2;

    public static final int BARRIER_RETRIEVE_SUCCESS = 0;
    public static final int BARRIER_NOT_FOUND = 1;
    public static final int BARRIER_RETRIEVE_ERROR = 2;
    public static final int BARRIER_WRONG = 3;

    public static final int PARKING_DONE = 0;
    public static final int PARKING_ERROR = 1;

    public static final int DRIVER_REGISTERED = 0;
    public static final int DRIVER_NOT_REGISTERED = 1;

}
