package br.ufrn.imd.smartparking.helpers;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import br.ufrn.imd.smartparking.domain.User;

/**
 * This class is used to initialize all the volley core objects.
 * It should be executed on app launch.
 *
 * This is a Singleton class.
 *
 * @author Rubem Kalebe
 * @version 07.03.2017
 */

public class SQLiteHandler extends SQLiteOpenHelper {

    // LogCat tag
    private static final String TAG = SQLiteHandler.class.getSimpleName();

    // Database Version
    private static final int DATABASE_VERSION = 2;

    // Database Name
    private static final String DATABASE_NAME = "SmartParkingDB";

    // Login table name
    private static final String TABLE_USER = "user";

    // Login Table Columns names
    private static final String ID_KEY = "id";
    private static final String FULL_NAME_KEY = "full_name";
    private static final String BIRTHDATE_KEY = "birthdate";
    private static final String PHONE_NUMBER_KEY = "phone_number";
    private static final String EMAIL_KEY = "email";
    private static final String GOOGLE_ID_KEY = "googleID";
    private static final String ID_DOCUMENT_KEY = "id_document";

    /**
     * Default constructor
     * @param context App context
     */
    public SQLiteHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_LOGIN_TABLE = "CREATE TABLE " + TABLE_USER + "("
                + ID_KEY + " INTEGER PRIMARY KEY,"
                + FULL_NAME_KEY + " TEXT,"
                + BIRTHDATE_KEY + " TEXT,"
                + PHONE_NUMBER_KEY + " TEXT,"
                + EMAIL_KEY + " TEXT,"
                + GOOGLE_ID_KEY + " TEXT,"
                + ID_DOCUMENT_KEY + " TEXT" + ")";
        db.execSQL(CREATE_LOGIN_TABLE);

        Log.d(TAG, "Database tables created.");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_USER);

        // Create tables again
        onCreate(db);
    }

    /**
     * Stores user info into the database.
     * @param u User to be added
     */
    public void addUser(User u) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(FULL_NAME_KEY, u.getFullName());
        values.put(BIRTHDATE_KEY, u.getBirthdate());
        values.put(PHONE_NUMBER_KEY, u.getPhoneNumber());
        values.put(EMAIL_KEY, u.getEmail());
        values.put(GOOGLE_ID_KEY, u.getGoogleID());
        values.put(ID_DOCUMENT_KEY, u.getIdDocument());

        long id = db.insert(TABLE_USER, null, values);
        db.close();

        Log.d(TAG, "New user inserted into SQLite: " + id);
    }

    /**
     * Retrieves user info from the database.
     * @return User object loaded with info
     */
    public User getUser() {
        User user = new User();
        String selectQuery = "SELECT  * FROM " + TABLE_USER;

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        cursor.moveToFirst();
        if(cursor.getCount() > 0) {
            user.setFullName(cursor.getString(1));
            user.setBirthdate(cursor.getString(2));
            user.setPhoneNumber(cursor.getString(3));
            user.setEmail(cursor.getString(4));
            user.setGoogleID(cursor.getString(5));
            user.setIdDocument(cursor.getString(6));

            Log.d(TAG, cursor.getString(1) + ";" + cursor.getString(2) + ";"
                    + cursor.getString(3) + ";" + cursor.getString(4) + ";"
                    + cursor.getString(5) + ";" + cursor.getString(6));
        }
        cursor.close();
        db.close();

        // return user
        Log.d(TAG, "Fetching user from SQLite: " + user.toString());

        return user;
    }

    /**
     * Clear the user table.
     */
    public void deleteUsers() {
        SQLiteDatabase db = this.getWritableDatabase();

        db.delete(TABLE_USER, null, null);
        db.close();

        Log.d(TAG, "All user info has been deleted from SQLite");
    }
}
