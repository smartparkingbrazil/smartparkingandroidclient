package br.ufrn.imd.smartparking.domain;

/**
 * Created by rubemkalebe on 30/10/16.
 */

public class EntityAttribute {

    private String name;
    private String type;
    private String value;


    public EntityAttribute() {
    }

    public EntityAttribute(String name, String type, String value) {
        this.name = name;
        this.type = type;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
