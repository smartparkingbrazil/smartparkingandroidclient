package br.ufrn.imd.smartparking.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * This class models a parking spot.
 * The attributes are based on spot entity attributes at Orion and data comes from Orion.
 *
 * @author Rubem Kalebe
 * @version 10.29.2016
 */

public class SpotEntity implements Serializable {

    private String type;
    private String isPattern;
    private String id;
    private List<EntityAttribute> attributes;
    private List<String> statusCode;


    public SpotEntity() {
        attributes = new ArrayList<EntityAttribute>();
        statusCode = new ArrayList<String>();
    }

    public SpotEntity(String type, String isPattern, String id, List<EntityAttribute> attributes, List<String> statusCode) {
        this.type = type;
        this.isPattern = isPattern;
        this.id = id;
        this.attributes = attributes;
        this.statusCode = statusCode;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getIsPattern() {
        return isPattern;
    }

    public void setIsPattern(String isPattern) {
        this.isPattern = isPattern;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<EntityAttribute> getAttributes() {
        return attributes;
    }

    public void setAttributes(List<EntityAttribute> attributes) {
        this.attributes = attributes;
    }

    public List<String> getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(List<String> statusCode) {
        this.statusCode = statusCode;
    }

    @Override
    public String toString() {
        return "SpotEntity{" +
                "type='" + type + '\'' +
                ", isPattern='" + isPattern + '\'' +
                ", id='" + id + '\'' +
                ", attributes=" + attributes +
                ", statusCode=" + statusCode +
                '}';
    }
}
