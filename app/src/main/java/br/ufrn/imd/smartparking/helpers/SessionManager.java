package br.ufrn.imd.smartparking.helpers;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import android.support.annotation.NonNull;


/**
 * This class maintains session data across the app by using SharedPreferences.
 * For that, we have a boolean flag, isLoggedIn, in the shared preferences in
 * order to check the login status.
 *
 * This is a Singleton class.
 *
 * @author Rubem Kalebe
 * @version 12.01.2016
 */

public class SessionManager {

    // LogCat tag
    private static final String TAG = SessionManager.class.getSimpleName();


    // SharedPreferences name
    public static final String PREFS_NAME = "SmartParkingPreferences";

    // Key for identify if the user has logged in the app
    private static final String IS_LOGGED_IN = "isLoggedIn";

    // App preferences/settings
    private SharedPreferences settings;

    // Single instance
    private static SessionManager instance;

    /**
     * Private constructor.
     * @param context App context
     */
    private SessionManager(@NonNull Context context) {
        this.settings = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
    }

    /**
     * Get single SessionManager instance.
     * @param context App context
     * @return A single SessionManager instance.
     */
    public static SessionManager getInstance(@NonNull Context context) {
        if(instance == null) {
            instance = new SessionManager(context);
        }
        return instance;
    }

    /**
     * Changes IS_LOGGED_IN attribute status.
     * @param isLoggedIn New status
     */
    public void setLogin(boolean isLoggedIn) {
        SharedPreferences.Editor editor = settings.edit();
        editor.putBoolean(IS_LOGGED_IN, isLoggedIn);
        editor.commit();
        Log.d(TAG, "User login session modified!");
    }

    /**
     *
     * @return True if the user has logged in the app and False, otherwise
     */
    public boolean isLoggedIn() {
        return this.settings.getBoolean(IS_LOGGED_IN, false);
    }

}
