package br.ufrn.imd.smartparking.utils;

import com.google.android.gms.maps.model.LatLng;

/**
 * Class to work with coordinates.
 *
 * @author Rubem Kalebe
 * @version 10.16.2016
 */

public class Coordinates {

    // CIVT-IMD coordinates
    public static final LatLng CIVT = new LatLng(-5.832407, -35.205447);

}
