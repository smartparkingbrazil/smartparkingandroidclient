package br.ufrn.imd.smartparking.domain;

import java.io.Serializable;

/**
 * This class models a parking spot.
 * The attributes are based on spot entity attributes at Orion and data comes from Orion.
 *
 * @author Rubem Kalebe
 * @version 11.09.2016
 */

public class Spot implements Serializable {

    // Spot id
    private String spotId;

    // Spot latitude
    private double latitude;

    // Spot longitude
    private double longitude;

    // Spot type
    private String type;

    // Spot status: if the spot is occupied or not
    private boolean isOccupied;

    // If the spot is occupied we need to know if the driver has identified himself
    private boolean driverIdentified;

    /**
     * Empty constructor
     */
    public Spot() {
        super();
    }

    /**
     * Constructor with parameters.
     * @param spotID Spot id
     * @param latitude Spot latitude
     * @param longitude Spot longitude
     * @param type Spot type
     * @param isOccupied Spot status: if the spot is occupied or not
     * @param driverIdentified If the spot is occupied we need to know if the driver has identified himself
     */
    public Spot(String spotID, double latitude, double longitude, String type, boolean isOccupied, boolean driverIdentified) {
        this.spotId = spotID;
        this.latitude = latitude;
        this.longitude = longitude;
        this.type = type;
        this.isOccupied = isOccupied;
        this.driverIdentified = driverIdentified;
    }

    /**
     *
     * @return Spot id
     */
    public String getSpotId() {
        return spotId;
    }

    /**
     *
     * @param spotId Spot id
     */
    public void setSpotId(String spotId) {
        this.spotId = spotId;
    }

    /**
     *
     * @return Spot latitude
     */
    public double getLatitude() {
        return latitude;
    }

    /**
     *
     * @param latitude Spot latitude
     */
    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    /**
     *
     * @return Spot longitude
     */
    public double getLongitude() {
        return longitude;
    }

    /**
     *
     * @param longitude Spot longitude
     */
    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    /**
     *
     * @return Spot type
     */
    public String getType() {
        return type;
    }

    /**
     *
     * @param type Spot type
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     *
     * @return Spot status: if the spot is occupied or not
     */
    public boolean isOccupied() {
        return isOccupied;
    }

    /**
     *
     * @param occupied Spot status: if the spot is occupied or not
     */
    public void setOccupied(boolean occupied) {
        isOccupied = occupied;
    }

    /**
     *
     * @return If the spot is occupied we need to know if the driver has identified himself
     */
    public boolean isDriverIdentified() {
        return driverIdentified;
    }

    /**
     *
     * @param driverIdentified If the spot is occupied we need to know if the driver has identified himself
     */
    public void setDriverIdentified(boolean driverIdentified) {
        this.driverIdentified = driverIdentified;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Spot spot = (Spot) o;

        if (Double.compare(spot.latitude, latitude) != 0) return false;
        return Double.compare(spot.longitude, longitude) == 0;

    }

}
