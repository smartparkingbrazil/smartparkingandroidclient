package br.ufrn.imd.smartparking.utils.validators;

/**
 * Check if a password input is valid.
 *
 * @author Rubem Kalebe
 * @version 10.21.2016
 */

public class CheckPassword {

    // Minimum of characters
    public static final int MIN = 4;

    /**
     * Check if a password input is valid.
     * For now, we just verify the string length.
     * @param password User password
     * @return True if date length is greater than the minimum and False, otherwise
     */
    public static boolean isValid(String password) {
        return password.length() >= MIN;
    }

    /**
     * Compare two passwords.
     * @param pass1 Original password
     * @param pass2 Repeated password
     * @return True if the passwords are equal and False, otherwise
     */
    public static boolean comparePasswords(String pass1, String pass2) {
        return pass1.compareTo(pass2) == 0;
    }

}
