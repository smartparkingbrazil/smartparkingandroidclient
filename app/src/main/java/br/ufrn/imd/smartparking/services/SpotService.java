package br.ufrn.imd.smartparking.services;

import android.app.Activity;
import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import br.ufrn.imd.smartparking.R;
import br.ufrn.imd.smartparking.domain.Spot;

/**
 * Created by rubemkalebe on 29/10/16.
 */

public class SpotService extends IntentService {

    public static final String SPOT_SERVICE = "br.ufrn.imd.smartparking.services.SpotService";
    public static final String SPOTS = "spots";
    public static final String RESULT = "result";

    private static final String TAG_APP = "SMART_PARKING";

    private RequestQueue requestQueue;
    private List<Spot> spots;

    private int result = Activity.RESULT_CANCELED;

    private boolean shouldRun = true;

    public SpotService() {
        super(TAG_APP);

        this.requestQueue = null;
        this.spots = new ArrayList<Spot>();

        Log.d("::DBG", "SpotService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        while (shouldRun) {
            Log.d("::DBG", "onHandleIntent");
            requestGetSpots();
            try {
                Thread.sleep(4000);
            } catch (InterruptedException e) {
                Log.d("THREAD::ERROR::SPOTS", e.toString());
            }
        }
    }

    private void requestGetSpots() {
        Log.d("::DBG", "Request Get Spots");

        String url = getResources().getString(R.string.host) + getResources().getString(R.string.spot_path);

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        /*spots = null;
                                JSONProcessor.toObject(response, SpotEntity.class);
                        result = Activity.RESULT_OK;
                        Log.d("::DBG", "Request Get Spots -- Publish");*/
                        result = Activity.RESULT_OK;
                        spots = generateSpotsFromJSON(response);
                        publishResults(result);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("REQ::ERROR::SPOTS", error.toString());
            }
        });

        getRequestQueue().add(stringRequest);
    }

    private List<Spot> generateSpotsFromJSON(String response) {
        List<Spot> spots = new ArrayList<Spot>();
        try {
            JSONObject jsonObject = new JSONObject(response);
            JSONArray contextResponses = jsonObject.getJSONArray("contextResponses");
            for(int i = 0; i < contextResponses.length(); i++) {
                try {
                    JSONObject contextElement = contextResponses.getJSONObject(i).getJSONObject("contextElement");
                    Log.d("::DBG", contextElement.toString());
                    JSONArray attributes = contextElement.getJSONArray("attributes");
                    Spot s = new Spot();
                    s.setSpotId(contextElement.getString("id"));
                    for(int j = 0; j < attributes.length(); j++) {
                        JSONObject attr = attributes.getJSONObject(j);
                        if(attr.getString("name").compareTo("latitude") == 0) {
                            s.setLatitude(attr.getDouble("value"));
                        } else if(attr.getString("name").compareTo("longitude") == 0) {
                            s.setLongitude(attr.getDouble("value"));
                        } else if(attr.getString("name").compareTo("type") == 0) {
                            s.setType(attr.getString("value"));
                        } else if(attr.getString("name").compareTo("isOccupied") == 0) {
                            s.setOccupied(attr.getBoolean("value"));
                        } else if(attr.getString("name").compareTo("driverIdentified") == 0) {
                            s.setDriverIdentified(attr.getBoolean("value"));
                        }
                    }
                    //Log.d("::DBG:SPOT", s.toString());
                    spots.add(s);
                } catch(JSONException e) {
                    // Inactive spot
                    break;
                }
            }
            return spots;
        } catch(Exception e) {
            e.printStackTrace();
        }
        return spots;
    }

    private RequestQueue getRequestQueue() {
        // lazy initialize the request queue, the queue instance will be
        // created when it is accessed for the first time
        if (requestQueue == null) {
            requestQueue = Volley.newRequestQueue(getApplicationContext());
        }
        return requestQueue;
    }

    private void publishResults(int result) {
        Log.d("::DBG", "Publish");
        Intent intent = new Intent(SPOT_SERVICE);
        Bundle b = new Bundle();

        b.putSerializable(SPOTS, (Serializable) this.spots);

        intent.putExtras(b);
        intent.putExtra(RESULT, result);
        sendBroadcast(intent);
    }

    @Override
    public void onDestroy() {
        Log.d("::DBG:Service", "onDestroy()");
        shouldRun = false;

        super.onDestroy();
    }
}
