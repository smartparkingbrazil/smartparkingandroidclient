package br.ufrn.imd.smartparking.utils.validators;

/**
 * Check if a CPF input is valid.
 *
 * @author Rubem Kalebe
 * @version 10.21.2016
 */

public class CheckCPF {

    // CPF length
    public static final int CPF_LENGTH = 11;

    /**
     * Algorithm to verify if some CPF is valid.
     * Based on modular arithmetic.
     * @param cpf CPF without punctuation
     * @return True if the CPF is valid and False, otherwise
     */
    public static boolean isValid(String cpf) {

        // Check length
        if(cpf.length() != CPF_LENGTH) {
            return false;
        }

        int[] numbers = new int[CPF_LENGTH];
        for(int i = 0; i < CPF_LENGTH; i++) {
            numbers[i] = Character.getNumericValue(cpf.charAt(i));
        }

        // Check equality
        // TODO
        /*if(numbers[0] == numbers[1] && numbers[1] == numbers[2] && numbers[2] == numbers[3] &&
                numbers[3] == numbers[4] && numbers[4] == numbers[5] && numbers[5] == numbers[6] &&
                numbers[6] == numbers[7] && numbers[7] == numbers[8] && numbers[8] == numbers[9] &&
                numbers[9] == numbers[10]) {
            return false;
        }*/

        // Check first digit
        int first = Character.getNumericValue(cpf.charAt(0))*10 +
                Character.getNumericValue(cpf.charAt(1))*9 +
                Character.getNumericValue(cpf.charAt(2))*8 +
                Character.getNumericValue(cpf.charAt(3))*7 +
                Character.getNumericValue(cpf.charAt(4))*6 +
                Character.getNumericValue(cpf.charAt(5))*5 +
                Character.getNumericValue(cpf.charAt(6))*4 +
                Character.getNumericValue(cpf.charAt(7))*3 +
                Character.getNumericValue(cpf.charAt(8))*2;
        if(((first * 10) % 11 == Character.getNumericValue(cpf.charAt(9))) ||
           ((first * 10) % 11 == 10 && Character.getNumericValue(cpf.charAt(9)) == 0)) {
            // do nothing
        } else {
            return false;
        }

        // Check second digit
        int second = Character.getNumericValue(cpf.charAt(0))*11 +
                Character.getNumericValue(cpf.charAt(1))*10 +
                Character.getNumericValue(cpf.charAt(2))*9 +
                Character.getNumericValue(cpf.charAt(3))*8 +
                Character.getNumericValue(cpf.charAt(4))*7 +
                Character.getNumericValue(cpf.charAt(5))*6 +
                Character.getNumericValue(cpf.charAt(6))*5 +
                Character.getNumericValue(cpf.charAt(7))*4 +
                Character.getNumericValue(cpf.charAt(8))*3 +
                Character.getNumericValue(cpf.charAt(9))*2;
        if(((first * 10) % 11 == Character.getNumericValue(cpf.charAt(9))) ||
                ((first * 10) % 11 == 10 && Character.getNumericValue(cpf.charAt(9)) == 0)) {
            return true;
        } else {
            return false;
        }
    }

}
