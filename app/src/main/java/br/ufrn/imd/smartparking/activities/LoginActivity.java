package br.ufrn.imd.smartparking.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.OptionalPendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import br.ufrn.imd.smartparking.R;
import br.ufrn.imd.smartparking.app.AppConfig;
import br.ufrn.imd.smartparking.app.AppController;
import br.ufrn.imd.smartparking.helpers.SQLiteHandler;
import br.ufrn.imd.smartparking.helpers.SessionManager;
import br.ufrn.imd.smartparking.utils.validators.CheckCPF;
import br.ufrn.imd.smartparking.utils.validators.CheckPassword;
import br.ufrn.imd.smartparking.domain.User;
import br.ufrn.imd.smartparking.views.MaskedEditText;

/**
 * This is the MAIN activity.
 * It is responsible for allow user login.
 * User can also sign up.
 * If the user has already logged in, this activity opens the ParkingMapActivity.
 *
 * @author Rubem Kalebe
 * @version 07.03.2017
 */
public class LoginActivity extends AppCompatActivity
        implements GoogleApiClient.OnConnectionFailedListener, View.OnClickListener {

    // LogCat tag
    private static final String TAG = LoginActivity.class.getSimpleName();

    // Intent to start ParkingMapActivity
    private static final int MAP_ACTIVITY_INTENT = 1;

    // Intent to Google sign in
    private static final int GOOGLE_SIGN_IN_INTENT = 2;

    // Variables for switching between login modes
    private LinearLayout inputFieldsLayout, loginOptions;
    private RelativeLayout signUpLayout;
    private boolean signInWithGoogleBool;

    // Google's sign in
    private SignInButton googleSignInButton;
    private GoogleApiClient mGoogleApiClient;

    // EditText to get user CPF
    private MaskedEditText CPFEditText;

    // EditText to get user password
    private EditText passEditText;

    // Button to confirm login
    private Button loginButton;

    // Object to manage user session
    private SessionManager sessionManager;

    // SQLite database handler
    private SQLiteHandler db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        inputFieldsLayout = (LinearLayout) findViewById(R.id.inputFieldsLayout);
        loginOptions = (LinearLayout) findViewById(R.id.loginOptionsLayout);
        signUpLayout = (RelativeLayout) findViewById(R.id.signUpLayout);
        signInWithGoogleBool = true;

        googleSignInButton = (SignInButton) findViewById(R.id.btn_google_sign_in);
        googleSignInButton.setOnClickListener(this);
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, (GoogleApiClient.OnConnectionFailedListener) this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();

        // Customizing G+ button
        googleSignInButton.setSize(SignInButton.SIZE_STANDARD);
        googleSignInButton.setScopes(gso.getScopeArray());

        CPFEditText = (MaskedEditText) findViewById(R.id.CPFEditText_signup);
        passEditText = (EditText) findViewById(R.id.passEditText_signup);
        loginButton = (Button) findViewById(R.id.loginButton);

        sessionManager = SessionManager.getInstance(getApplicationContext());

        db = new SQLiteHandler(getApplicationContext());

        // Check if user has already logged in or not
        if(sessionManager.isLoggedIn()) {
            // If user has already logged in, open the map
            openMap();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        // Open last login mode
        updateUI();
    }

    public void googleSignIn() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, GOOGLE_SIGN_IN_INTENT);
    }

    private void googleSignOut() {
        Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(
                new ResultCallback<Status>() {
                    @Override
                    public void onResult(Status status) {
                        // TODO
                    }
                });
    }

    private void googleRevokeAccess() {
        Auth.GoogleSignInApi.revokeAccess(mGoogleApiClient).setResultCallback(
                new ResultCallback<Status>() {
                    @Override
                    public void onResult(Status status) {
                        // TODO
                    }
                });
    }

    private void handleGoogleSignInResult(GoogleSignInResult result) {
        if (result.isSuccess()) {
            // Signed in successfully, show authenticated UI.
            GoogleSignInAccount acct = result.getSignInAccount();

            Log.e(TAG, "display name: " + acct.getDisplayName());

            final String googleID = acct.getId();
            final String personName = acct.getDisplayName();
            final String email = acct.getEmail();
            //String personPhotoUrl = acct.getPhotoUrl().toString();

            final ProgressDialog progressDialog = new ProgressDialog(this);
            progressDialog.setIndeterminate(true);
            progressDialog.setMessage(getString(R.string.processing));
            progressDialog.setCancelable(false);
            progressDialog.show();

            Map<String, String> params = new HashMap<String, String>();
            params.put(AppConfig.GOOGLE_ID_KEY, googleID);

            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST,
                    AppConfig.URL_GET_DRIVER, new JSONObject(params), new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    Log.d(TAG, "GetDriver response: " + response.toString());
                    progressDialog.dismiss();

                    try {
                        int error = response.getInt(AppConfig.ERROR_CODE_KEY);

                        if(error == AppConfig.DRIVER_REGISTERED) {
                            sessionManager.setLogin(true);
                            JSONObject userJson = response.getJSONObject(AppConfig.DRIVER_KEY);
                            db.addUser(new User(userJson.getString(AppConfig.FULL_NAME_KEY),
                                    userJson.getString(AppConfig.BIRTHDATE_KEY),
                                    userJson.getString(AppConfig.PHONE_NUMBER_KEY),
                                    userJson.getString(AppConfig.EMAIL_KEY),
                                    userJson.getString(AppConfig.GOOGLE_ID_KEY),
                                    userJson.getString(AppConfig.ID_DOCUMENT_KEY)));
                            openMap();
                        } else if(error == AppConfig.DRIVER_NOT_REGISTERED) {
                            Toast.makeText(getApplicationContext(), R.string.complete_registation, Toast.LENGTH_SHORT).show();
                            completeRegistration(googleID, personName, email);
                        } else {
                            Toast.makeText(getApplicationContext(), R.string.houston_error_msg, Toast.LENGTH_SHORT).show();
                        }
                    } catch(JSONException e) {
                        // JSON Error
                        e.printStackTrace();
                        Toast.makeText(getApplicationContext(), R.string.houston_error_msg, Toast.LENGTH_SHORT).show();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e(TAG, "GetDriver Error: " + error.getMessage());
                    Toast.makeText(getApplicationContext(), R.string.houston_error_msg, Toast.LENGTH_SHORT).show();
                    progressDialog.dismiss();
                }
            }) {
                /**
                 * Passing some request headers
                 *
                 */
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<String, String>();
                    headers.put("Content-Type", "application/json; charset=utf-8");
                    return headers;
                }
            };

            AppController.getInstance().addToRequestQueue(jsonObjectRequest, AppConfig.REQUEST_DRIVER);
        } else {
            // TODO
        }

    }

    /**
     * Action executed when loginButton is touched.
     * @param view loginButton
     */
    public void login(View view) {

        if(!validateLogin()) {
            onLoginFailed();
            return;
        }

        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage(getString(R.string.authenticating));
        progressDialog.setCancelable(false);
        progressDialog.show();

        Map<String, String> params = new HashMap<String, String>();
        params.put(AppConfig.ID_DOCUMENT_KEY, CPFEditText.getText(true).toString());
        params.put(AppConfig.PASSWORD_KEY, passEditText.getText().toString());

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST,
                AppConfig.URL_LOGIN, new JSONObject(params), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d(TAG, "Login response: " + response.toString());
                progressDialog.dismiss();

                try {
                    int error = response.getInt(AppConfig.ERROR_CODE_KEY);

                    if(error == AppConfig.DRIVER_RETRIEVE_SUCCESS) {
                        sessionManager.setLogin(true);
                        JSONObject userJson = response.getJSONObject(AppConfig.DRIVER_KEY);
                        db.addUser(new User(userJson.getString(AppConfig.FULL_NAME_KEY),
                                userJson.getString(AppConfig.BIRTHDATE_KEY),
                                userJson.getString(AppConfig.PHONE_NUMBER_KEY),
                                userJson.getString(AppConfig.EMAIL_KEY),
                                userJson.getString(AppConfig.GOOGLE_ID_KEY),
                                userJson.getString(AppConfig.ID_DOCUMENT_KEY)));
                        openMap();
                    } else if(error == AppConfig.DRIVER_NOT_FOUND) {
                        Toast.makeText(getApplicationContext(), R.string.driver_not_found_mdg, Toast.LENGTH_SHORT).show();
                        CPFEditText.setError(getString(R.string.driver_not_found_mdg));
                    } else if(error == AppConfig.DRIVER_WRONG_PASS) {
                        Toast.makeText(getApplicationContext(), R.string.wrong_pass_msg, Toast.LENGTH_SHORT).show();
                        passEditText.setError(getString(R.string.wrong_pass_msg));
                    } else {
                        Toast.makeText(getApplicationContext(), R.string.houston_error_msg, Toast.LENGTH_SHORT).show();
                    }
                } catch(JSONException e) {
                    // JSON Error
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), R.string.houston_error_msg, Toast.LENGTH_SHORT).show();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Login Error: " + error.getMessage());
                Toast.makeText(getApplicationContext(), R.string.houston_error_msg, Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }
        }) {
            /**
             * Passing some request headers
             * */
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                return headers;
            }
        };

        AppController.getInstance().addToRequestQueue(jsonObjectRequest, AppConfig.REQUEST_LOGIN);
    }

    private boolean validateLogin() {

        String cpf = CPFEditText.getText(true).toString();
        String password = passEditText.getText().toString();

        // Check CPF
        if(!CheckCPF.isValid(cpf)) {
            CPFEditText.setError(getString(R.string.invalid_cpf));
            return false;
        } else {
            CPFEditText.setError(null);
        }

        // Check password
        if(!CheckPassword.isValid(password)) {
            passEditText.setError(getString(R.string.invalid_password));
            return false;
        } else {
            passEditText.setError(null);
        }

        return true;
    }

    private void onLoginFailed() {
        loginButton.setEnabled(true);
    }

    private void onLoginSuccess() {
        loginButton.setEnabled(true);
        openMap();
    }

    /**
     * Start ParkingMapActivity.
     */
    private void openMap() {
        Intent i = new Intent(this, ParkingMapActivity.class);
        startActivityForResult(i, MAP_ACTIVITY_INTENT);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if((requestCode == MAP_ACTIVITY_INTENT) && (resultCode == ParkingMapActivity.RESULT_FINISH)) {
            // Finish app request
            finish();
        } else if((requestCode == MAP_ACTIVITY_INTENT) && (resultCode == ParkingMapActivity.RESULT_LOGOUT)) {
            // Logout request
            sessionManager.setLogin(false);
            db.deleteUsers();

            // Show logout message
            Toast toast = Toast.makeText(getApplicationContext(), R.string.logout_successful_msg, Toast.LENGTH_SHORT);
            toast.show();
        } else if(requestCode == GOOGLE_SIGN_IN_INTENT) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleGoogleSignInResult(result);
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    /**
     * Action executed when signupTextView is touched
     * @param view signupTextView
     */
    public void signup(View view) {
        Bundle bundle = new Bundle();
        bundle.putBoolean(AppConfig.SIGN_IN_WITH_GOOGLE, signInWithGoogleBool);
        Intent i = new Intent(this, SignUpActivity.class);
        i.putExtras(bundle);
        startActivity(i);
    }

    public void completeRegistration(String googleID, String personName, String email) {
        Bundle bundle = new Bundle();
        bundle.putBoolean(AppConfig.SIGN_IN_WITH_GOOGLE, signInWithGoogleBool);
        bundle.putString(AppConfig.GOOGLE_ID_KEY, googleID);
        bundle.putString(AppConfig.FULL_NAME_KEY, personName);
        bundle.putString(AppConfig.EMAIL_KEY, email);
        Intent i = new Intent(this, SignUpActivity.class);
        i.putExtras(bundle);
        startActivity(i);
    }

    public void signInWithoutGoogle(View view) {
        signInWithGoogleBool = false;
        updateUI();
    }

    private void updateUI() {
        if(signInWithGoogleBool) {
            inputFieldsLayout.setVisibility(View.GONE);
            signUpLayout.setVisibility(View.GONE);
            loginOptions.setVisibility(View.VISIBLE);
        } else {
            inputFieldsLayout.setVisibility(View.VISIBLE);
            signUpLayout.setVisibility(View.VISIBLE);
            loginOptions.setVisibility(View.GONE);
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        // An unresolvable error has occurred and Google APIs (including Sign-In) will not
        // be available.
        Log.d(TAG, "onConnectionFailed:" + connectionResult);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();

        switch(id) {
            case R.id.btn_google_sign_in:
                googleSignIn();
                break;
        }
    }
}
