package br.ufrn.imd.smartparking.app;

import android.app.Application;
import android.text.TextUtils;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

/**
 * This class is used to initialize all the volley core objects.
 * It should be executed on app launch.
 *
 * This is a Singleton class.
 *
 * @author Rubem Kalebe (based on http://www.androidhive.info/2012/01/android-login-and-registration-with-php-mysql-and-sqlite/)
 * @version 12.01.2016
 */

public class AppController extends Application {

    // LogCat tag
    private static final String TAG = AppController.class.getSimpleName();

    // RequestQueue
    private RequestQueue requestQueue;

    // Single instance
    private static AppController instance;

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
    }

    /**
     *
     * @return A single AppController instance.
     */
    public static synchronized AppController getInstance() {
        return instance;
    }

    /**
     *
     * @return the request queue
     */
    public RequestQueue getRequestQueue() {
        if(requestQueue == null) {
            requestQueue = Volley.newRequestQueue(getApplicationContext());
        }
        return requestQueue;
    }

    /**
     * Add request to request queue.
     * @param request Request
     * @param tag Request tag
     */
    public <T> void addToRequestQueue(Request<T> request, String tag) {
        request.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
        getRequestQueue().add(request);
    }

    /**
     * Add request to request queue.
     * @param request Request
     */
    public <T> void addToRequestQueue(Request<T> request) {
        request.setTag(TAG);
        getRequestQueue().add(request);
    }

    /**
     * Cancel pending requests from a tag.
     * @param tag requests tag
     */
    public void cancelPendingRequests(Object tag) {
        if(requestQueue != null) {
            requestQueue.cancelAll(tag);
        }
    }
}
