package br.ufrn.imd.smartparking.activities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import br.ufrn.imd.smartparking.R;
import br.ufrn.imd.smartparking.app.AppController;
import br.ufrn.imd.smartparking.helpers.SQLiteHandler;
import br.ufrn.imd.smartparking.helpers.SessionManager;
import br.ufrn.imd.smartparking.domain.Spot;
import br.ufrn.imd.smartparking.domain.SpotMarker;
import br.ufrn.imd.smartparking.services.SpotService;
import br.ufrn.imd.smartparking.app.AppConfig;
import br.ufrn.imd.smartparking.utils.Coordinates;
import br.ufrn.imd.smartparking.domain.User;

/**
 * This activity shows the parking map and the spots in real time.
 * The user can also authorize his entry, linkage to a spot, find the closest spot.
 * Here the user can change some config.
 * The user can also log out.
 *
 * @author Rubem Kalebe
 * @version 07.01.2017
 */
public class ParkingMapActivity extends AppCompatActivity implements OnMapReadyCallback {

    // LogCat Tag
    private static final String TAG = ParkingMapActivity.class.getSimpleName();

    // This code indicates that the user wants close the app
    public static final int RESULT_FINISH = 0;

    // This code indicates that the user wants log out
    public static final int RESULT_LOGOUT = 1;

    // Intent to start ReadQRActivity for reading QR
    private static final int READ_QR_INTENT = 1;

    // Google map object
    private GoogleMap googleMap;

    // Map fragment to show the map
    private MapFragment mapFragment;

    private List<SpotMarker> markers;

    private SessionManager sessionManager;

    private SQLiteHandler db;

    private BroadcastReceiver receiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_parking_map);

        mapFragment = (MapFragment) getFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        markers = new ArrayList<SpotMarker>();

        sessionManager = SessionManager.getInstance(getApplicationContext());

        db = new SQLiteHandler(getApplicationContext());

        if(!sessionManager.isLoggedIn()) {
            // Bug fixed: logout did not work well when changing screen orientation
            logout();
        } else {
            // Show an intro message
            User user = db.getUser();
            Toast toast = Toast.makeText(getApplicationContext(), getString(R.string.hello_formatted) + user.getFullName().split(" ")[0] + "!", Toast.LENGTH_SHORT);
            toast.show();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onResume() {
        super.onResume();

        receiver = new BroadcastReceiver() {

            @Override
            public void onReceive(Context context, Intent intent) {
                Bundle bundle = intent.getExtras();
                Log.d("::DBG", "Receiver");
                if (bundle != null) {
                    int resultCode = bundle.getInt(SpotService.RESULT);
                    if (resultCode == RESULT_OK) {
                        List<Spot> spots = (List<Spot>) bundle.getSerializable(SpotService.SPOTS);
                        if(markers.size() == 0) {
                            for(Spot spot : spots) {
                                Marker marker = googleMap.addMarker(new MarkerOptions().
                                        position(new LatLng(spot.getLatitude(), spot.getLongitude())).
                                        title("Vaga " + (markers.size() + 1)));
                                markers.add(new SpotMarker(spot, marker));
                            }
                        } else {
                            for(int i = 0; i < markers.size(); i++) {
                                for(Spot spot : spots) {
                                    if(markers.get(i).getSpot().equals(spot)) {
                                        markers.get(i).setSpot(spot);
                                        break;
                                    }
                                }
                            }
                        }
                        updateMap();
                    }
                }
            }
        };

        registerReceiver(receiver, new IntentFilter(SpotService.SPOT_SERVICE));
        Intent i = new Intent(this, SpotService.class);
        startService(i);
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d(TAG, "onPause()");

        if(receiver != null) {
            unregisterReceiver(receiver);
            receiver = null;
        }

        Intent i = new Intent(this, SpotService.class);
        stopService(i);
    }

    private void updateMap() {
        if (this.googleMap != null) {
            for(int i = 0; i < markers.size(); i++) {
                if(((markers.get(i).getSpot().getType().compareTo("elderly") == 0) ||
                        (markers.get(i).getSpot().getType().compareTo("handicapped") == 0)) &&
                        !markers.get(i).getSpot().isOccupied()) {
                    markers.get(i).getMarker().setIcon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE));
                } else if(markers.get(i).getSpot().isOccupied()) {
                    markers.get(i).getMarker().setIcon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));
                } else {
                    markers.get(i).getMarker().setIcon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN));
                }
            }
        } else {
            Log.i("UPDATE::MAP", "ERRO");
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.parking_map_actions, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;
        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(Coordinates.CIVT, 20));
        googleMap.animateCamera(CameraUpdateFactory.zoomTo(18.8f), 250, null);
        googleMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()) {
            case R.id.settings:
                settings();
                return true;
            case R.id.logout:
                logout();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void settings() {
        // TODO
        Toast toast = Toast.makeText(getApplicationContext(), getString(R.string.account_settings), Toast.LENGTH_SHORT);
        toast.show();
    }

    private void logout() {
        // Finish activity sending logout request
        this.setResult(RESULT_LOGOUT);
        finish();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        this.setResult(RESULT_FINISH);
        finish();
    }

    public void readQR(View view) {
        Intent i = new Intent(this, ReadQRActivity.class);
        startActivityForResult(i, READ_QR_INTENT);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if(requestCode == READ_QR_INTENT && resultCode == Activity.RESULT_OK) {
            String qr = data.getStringExtra(AppConfig.QR_CODE);
            try {
                JSONObject jsonQR = new JSONObject(qr);
                String action = jsonQR.getString("action");
                switch (action) {
                    case "entry":
                        entry(qr);
                        break;
                    case "park":
                        park(qr);
                        break;
                    case "exit":
                        exit(qr);
                        break;
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    private void entry(String qr) {
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage(getString(R.string.processing));
        progressDialog.setCancelable(false);
        progressDialog.show();

        Map<String, String> params = new HashMap<String, String>();
        try {
            JSONObject jsonQR = new JSONObject(qr);
            params.put(AppConfig.ID_KEY, jsonQR.getString("id"));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST,
                AppConfig.URL_ENTRY, new JSONObject(params), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d(TAG, "Entry response: " + response.toString());
                progressDialog.dismiss();

                try {
                    int error = response.getInt(AppConfig.ERROR_CODE_KEY);

                    if(error == AppConfig.BARRIER_RETRIEVE_SUCCESS) {
                        Toast.makeText(getApplicationContext(), R.string.entry_ok, Toast.LENGTH_LONG).show();
                    } else if(error == AppConfig.BARRIER_WRONG) {
                        Toast.makeText(getApplicationContext(), R.string.wrong_barrier_entry, Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(getApplicationContext(), R.string.houston_error_msg, Toast.LENGTH_LONG).show();
                    }
                } catch(JSONException e) {
                    // JSON Error
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), R.string.houston_error_msg, Toast.LENGTH_LONG).show();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Entry Error: " + error.getMessage());
                Toast.makeText(getApplicationContext(), R.string.houston_error_msg, Toast.LENGTH_LONG).show();
                progressDialog.dismiss();
            }
        }) {
            /**
             * Passing some request headers
             * */
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                return headers;
            }
        };

        AppController.getInstance().addToRequestQueue(jsonObjectRequest, AppConfig.REQUEST_ENTRY);
    }

    private void park(String qr) {
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage(getString(R.string.processing));
        progressDialog.setCancelable(false);
        progressDialog.show();

        Map<String, String> params = new HashMap<String, String>();
        try {
            JSONObject jsonQR = new JSONObject(qr);
            params.put(AppConfig.SPOT_ID_KEY, jsonQR.getString("id"));
            params.put(AppConfig.DRIVER_ID_KEY, db.getUser().getIdDocument());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST,
                AppConfig.URL_PARK, new JSONObject(params), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d(TAG, "Park response: " + response.toString());
                progressDialog.dismiss();

                try {
                    int error = response.getInt(AppConfig.ERROR_CODE_KEY);

                    if(error == AppConfig.PARKING_DONE) {
                        Toast.makeText(getApplicationContext(), R.string.parking_ok, Toast.LENGTH_LONG).show();
                    } else if(error == AppConfig.PARKING_ERROR) {
                        Toast.makeText(getApplicationContext(), R.string.parking_denied, Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(getApplicationContext(), R.string.houston_error_msg, Toast.LENGTH_LONG).show();
                    }
                } catch(JSONException e) {
                    // JSON Error
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), R.string.houston_error_msg, Toast.LENGTH_LONG).show();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Park Error: " + error.getMessage());
                Toast.makeText(getApplicationContext(), R.string.houston_error_msg, Toast.LENGTH_LONG).show();
                progressDialog.dismiss();
            }
        }) {
            /**
             * Passing some request headers
             * */
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                return headers;
            }
        };

        AppController.getInstance().addToRequestQueue(jsonObjectRequest, AppConfig.REQUEST_PARK);
    }

    private void exit(String qr) {
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage(getString(R.string.processing));
        progressDialog.setCancelable(false);
        progressDialog.show();

        Map<String, String> params = new HashMap<String, String>();
        try {
            JSONObject jsonQR = new JSONObject(qr);
            params.put(AppConfig.ID_KEY, jsonQR.getString("id"));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST,
                AppConfig.URL_EXIT, new JSONObject(params), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d(TAG, "Exit response: " + response.toString());
                progressDialog.dismiss();

                try {
                    int error = response.getInt(AppConfig.ERROR_CODE_KEY);

                    if(error == AppConfig.BARRIER_RETRIEVE_SUCCESS) {
                        Toast.makeText(getApplicationContext(), R.string.exit_ok, Toast.LENGTH_LONG).show();
                    } else if(error == AppConfig.BARRIER_WRONG) {
                        Toast.makeText(getApplicationContext(), R.string.wrong_barrier_exit, Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(getApplicationContext(), R.string.houston_error_msg, Toast.LENGTH_LONG).show();
                    }
                } catch(JSONException e) {
                    // JSON Error
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), R.string.houston_error_msg, Toast.LENGTH_LONG).show();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Exit Error: " + error.getMessage());
                Toast.makeText(getApplicationContext(), R.string.houston_error_msg, Toast.LENGTH_LONG).show();
                progressDialog.dismiss();
            }
        }) {
            /**
             * Passing some request headers
             * */
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                return headers;
            }
        };

        AppController.getInstance().addToRequestQueue(jsonObjectRequest, AppConfig.REQUEST_EXIT);
    }

}
