package br.ufrn.imd.smartparking.domain;

import com.google.android.gms.maps.model.Marker;

import java.io.Serializable;

/**
 * This class represents an app spot marker.
 *
 * @author Rubem Kalebe
 * @version 10.29.2016
 */

public class SpotMarker implements Serializable {

    // Spot object
    private Spot spot;

    // Marker object
    private Marker marker;


    /**
     * Empty constructor.
     */
    public SpotMarker() {

    }

    /**
     * Constructor with parameters.
     * @param spot Spot object
     * @param marker Marker object
     */
    public SpotMarker(Spot spot, Marker marker) {
        this.spot = spot;
        this.marker = marker;
    }

    /**
     *
     * @return Spot object
     */
    public Spot getSpot() {
        return spot;
    }

    /**
     *
     * @param spot Spot object
     */
    public void setSpot(Spot spot) {
        this.spot = spot;
    }

    /**
     *
     * @return Marker object
     */
    public Marker getMarker() {
        return marker;
    }

    /**
     *
     * @param marker Marker object
     */
    public void setMarker(Marker marker) {
        this.marker = marker;
    }
}
