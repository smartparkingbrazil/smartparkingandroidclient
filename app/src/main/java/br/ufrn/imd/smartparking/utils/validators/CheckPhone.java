package br.ufrn.imd.smartparking.utils.validators;

/**
 * Check if a phone input is valid.
 *
 * @author Rubem Kalebe
 * @version 10.21.2016
 */

public class CheckPhone {

    /**
     * Check if a phone input is valid.
     * For now, we just verify the string length.
     * @param phone User phone number
     * @return True if date length is greater than the minimum and False, otherwise
     */
    public static boolean isValid(String phone) {
        return phone.length() > 0;
    }

}
