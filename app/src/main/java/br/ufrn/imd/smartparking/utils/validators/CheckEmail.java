package br.ufrn.imd.smartparking.utils.validators;

/**
 * Check if an email input is valid.
 *
 * @author Rubem Kalebe
 * @version 07.04.2017
 */

public class CheckEmail {

    /**
     * Check if an email input is valid.
     * @param email User's email
     * @return
     */
    public static boolean isValid(String email) {
        return email.length() > 0 && email.contains("@");
    }

}
