package br.ufrn.imd.smartparking.activities;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.PhoneNumberFormattingTextWatcher;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import br.ufrn.imd.smartparking.R;
import br.ufrn.imd.smartparking.app.AppConfig;
import br.ufrn.imd.smartparking.app.AppController;
import br.ufrn.imd.smartparking.domain.User;
import br.ufrn.imd.smartparking.utils.validators.CheckBirthdate;
import br.ufrn.imd.smartparking.utils.validators.CheckCPF;
import br.ufrn.imd.smartparking.utils.validators.CheckEmail;
import br.ufrn.imd.smartparking.utils.validators.CheckName;
import br.ufrn.imd.smartparking.utils.validators.CheckPassword;
import br.ufrn.imd.smartparking.utils.validators.CheckPhone;
import br.ufrn.imd.smartparking.views.MaskedEditText;

/**
 * This activity is responsible for user register.
 *
 * @author Rubem Kalebe
 * @version 07.04.2017
 */
public class SignUpActivity extends AppCompatActivity {

    // LogCat Tag
    private static final String TAG = SignUpActivity.class.getSimpleName();

    // Fields
    private EditText nameEditText;
    private MaskedEditText birthDateEditText;
    private MaskedEditText phoneEditText;
    private MaskedEditText CPFEditText;
    private EditText emailEditText;
    private EditText passEditText;
    private EditText repeatedPassEditText;
    private RadioGroup handicappedRadioGroup;
    private RadioButton isHandicappedButton;
    private RadioButton isNotHandicappedButton;
    private Button confirmButton;
    private boolean isHandicapped;

    private View ruleView;
    private RelativeLayout signInLayout;

    private String googleID;
    private boolean signInWithGoogleBool;

    // Dialog para escolha de data
    private DatePickerDialog datePickerDialog;

    // Formatador para a data escolhida pelo user
    private SimpleDateFormat simpleDateFormat;

    private static final int MINIMUM_AGE = 18;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);

        nameEditText = (EditText) findViewById(R.id.nameEditText);
        birthDateEditText = (MaskedEditText) findViewById(R.id.birthEditText);
        birthDateEditText.setInputType(InputType.TYPE_NULL);
        phoneEditText = (MaskedEditText) findViewById(R.id.phoneEditText);
        emailEditText = (EditText) findViewById(R.id.emailEditText);
        CPFEditText = (MaskedEditText) findViewById(R.id.CPFEditText_signup);
        passEditText = (EditText) findViewById(R.id.passEditText_signup);
        repeatedPassEditText = (EditText) findViewById(R.id.repeatedPassEditText);
        handicappedRadioGroup = (RadioGroup) findViewById(R.id.handicappedRadioGroup);
        isHandicappedButton = (RadioButton) findViewById(R.id.isHandicappedBt);
        isNotHandicappedButton = (RadioButton) findViewById(R.id.isNotHandicappedBt);
        confirmButton = (Button) findViewById(R.id.confirmButton);
        isHandicapped = false;

        ruleView = (View) findViewById(R.id.ruleView);
        signInLayout = (RelativeLayout) findViewById(R.id.signInLayout);

        Intent i = getIntent();
        Bundle bundle = i.getExtras();
        signInWithGoogleBool = bundle.getBoolean(AppConfig.SIGN_IN_WITH_GOOGLE);
        if(signInWithGoogleBool) {
            String name = bundle.getString(AppConfig.FULL_NAME_KEY);
            String email = bundle.getString(AppConfig.EMAIL_KEY);
            googleID = bundle.getString(AppConfig.GOOGLE_ID_KEY);

            nameEditText.setText(name);
            emailEditText.setText(email);
            nameEditText.setEnabled(false);
            emailEditText.setEnabled(false);
            passEditText.setText("");
            repeatedPassEditText.setText("");
            passEditText.setVisibility(View.GONE);
            repeatedPassEditText.setVisibility(View.GONE);
            ruleView.setVisibility(View.GONE);
            signInLayout.setVisibility(View.GONE);
        } else {
            googleID = "";
        }

        // Formato aqui no Brasil
        simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");

        // Selecao de data
        Calendar newDate = Calendar.getInstance();
        datePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            // Captura a data selecionada pelo user
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar date = Calendar.getInstance();
                date.set(year, monthOfYear, dayOfMonth);
                birthDateEditText.setText(simpleDateFormat.format(date.getTime()));
            }
        }, newDate.get(Calendar.YEAR), newDate.get(Calendar.MONTH), newDate.get(Calendar.DAY_OF_MONTH));

        // Minimo de 18 anos
        Calendar c = Calendar.getInstance();
        c.setTime(new Date());
        c.add(Calendar.YEAR, -MINIMUM_AGE); // Subtract 18 years
        long maxDate = c.getTime().getTime();
        datePickerDialog.getDatePicker().setMaxDate(maxDate);

        // Garante a exibição do date picker dialog
        birthDateEditText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(hasFocus) {
                    datePickerDialog.show();
                }
            }
        });
        birthDateEditText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                datePickerDialog.show();
            }
        });

        phoneEditText.addTextChangedListener(new PhoneNumberFormattingTextWatcher());
    }

    /**
     * Action performed when confirmButton is touched.
     * It shows an AlertDialog to confirm operation.
     * @param view confirmButton
     */
    public void signUp(View view) {
        if(!validateLogin()) {
            onRegisterFailed();
            return;
        }

        final ProgressDialog progressDialog = new ProgressDialog(this);
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setTitle(getString(R.string.warning));
        alert.setMessage(getString(R.string.dialog_signup_msg));
        alert.setCancelable(false);

        alert.setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                progressDialog.setIndeterminate(true);
                progressDialog.setMessage(getString(R.string.creating_account));
                progressDialog.setCancelable(false);
                progressDialog.show();

                Map<String, String> params = new HashMap<String, String>();
                params.put(AppConfig.FULL_NAME_KEY, nameEditText.getText().toString());
                params.put(AppConfig.BIRTHDATE_KEY, birthDateEditText.getText(false).toString());
                params.put(AppConfig.PHONE_NUMBER_KEY, phoneEditText.getText(true).toString());
                params.put(AppConfig.EMAIL_KEY, emailEditText.getText().toString());
                params.put(AppConfig.GOOGLE_ID_KEY, googleID); // Sign up via app or via Google's button
                params.put(AppConfig.ID_DOCUMENT_KEY, CPFEditText.getText(true).toString());
                params.put(AppConfig.PASSWORD_KEY, passEditText.getText().toString());
                params.put(AppConfig.HAS_DISABILITY_KEY, Boolean.toString(isHandicapped));

                JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST,
                        AppConfig.URL_REGISTER, new JSONObject(params), new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d(TAG, "SignUp response: " + response.toString());
                        progressDialog.dismiss();

                        try {
                            int error = response.getInt(AppConfig.ERROR_CODE_KEY);

                            if(error == AppConfig.DRIVER_INSERT_SUCCESS) {
                                Toast.makeText(getApplicationContext(), R.string.driver_registered, Toast.LENGTH_SHORT).show();
                                finish();
                            } else if(error == AppConfig.DRIVER_INSERT_DUPLICATE) {
                                Toast.makeText(getApplicationContext(), R.string.id_already_in_use, Toast.LENGTH_SHORT).show();
                                CPFEditText.setError(getString(R.string.id_already_in_use));
                            } else {
                                Toast.makeText(getApplicationContext(), R.string.houston_error_msg, Toast.LENGTH_SHORT).show();
                            }
                        } catch(JSONException e) {
                            // JSON Error
                            e.printStackTrace();
                            Toast.makeText(getApplicationContext(), R.string.houston_error_msg, Toast.LENGTH_SHORT).show();
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e(TAG, "SignUp Error: " + error.getMessage());
                        Toast.makeText(getApplicationContext(), R.string.houston_error_msg, Toast.LENGTH_SHORT).show();
                        progressDialog.dismiss();
                    }
                }) {
                    /**
                     * Passing some request headers
                     *
                     */
                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        HashMap<String, String> headers = new HashMap<String, String>();
                        headers.put("Content-Type", "application/json; charset=utf-8");
                        return headers;
                    }
                };

                AppController.getInstance().addToRequestQueue(jsonObjectRequest, AppConfig.REQUEST_SIGNUP);

            }
        });
        alert.setNegativeButton(getString(R.string.no), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        // Show alert dialog
        alert.show();
    }

    private boolean validateLogin() {
        String name = nameEditText.getText().toString();
        String birthdate = birthDateEditText.getText(true).toString(); // only numbers
        String phone = phoneEditText.getText(true).toString(); // only numbers
        String email = emailEditText.getText().toString();
        String cpf = CPFEditText.getText(true).toString(); // only numbers
        String pass1 = passEditText.getText().toString();
        String pass2 = repeatedPassEditText.getText().toString();

        if(handicappedRadioGroup.getCheckedRadioButtonId() == isHandicappedButton.getId()) {
            isHandicapped = true;
        } else if(handicappedRadioGroup.getCheckedRadioButtonId() == isNotHandicappedButton.getId()){
            isHandicapped = false;
        }

        // Check name
        if(!CheckName.isValid(name)) {
            nameEditText.setError(getString(R.string.invalid_name));
            return false;
        } else {
            nameEditText.setError(null);
        }

        // Check birthdate
        if(!CheckBirthdate.isValid(birthdate)) {
            birthDateEditText.setError(getString(R.string.invalid_birthdate));
            return false;
        } else {
            birthDateEditText.setError(null);
        }

        // Check phone
        if(!CheckPhone.isValid(phone)) {
            phoneEditText.setError(getString(R.string.invalid_phone));
            return false;
        } else {
            phoneEditText.setError(null);
        }

        // Check email
        if(!CheckEmail.isValid(email)) {
            emailEditText.setError(getString(R.string.invalid_email));
            return false;
        } else {
            emailEditText.setError(null);
        }

        // Check CPF
        if(!CheckCPF.isValid(cpf)) {
            CPFEditText.setError(getString(R.string.invalid_cpf));
            return false;
        } else {
            CPFEditText.setError(null);
        }

        // Check pass1
        if(!CheckPassword.isValid(pass1) && !signInWithGoogleBool) {
            passEditText.setError(getString(R.string.invalid_password));
            return false;
        } else {
            passEditText.setError(null);
        }

        // Check pass2
        if(!CheckPassword.isValid(pass2) && !signInWithGoogleBool) {
            repeatedPassEditText.setError(getString(R.string.invalid_password));
            return false;
        } else {
            repeatedPassEditText.setError(null);
        }

        // Check the passwords
        if(!CheckPassword.comparePasswords(pass1, pass2) && !signInWithGoogleBool) {
            passEditText.setError(getString(R.string.passwords_dont_match));
            repeatedPassEditText.setError(getString(R.string.passwords_dont_match));
            return false;
        } else {
            passEditText.setError(null);
            repeatedPassEditText.setError(null);
        }

        return true;
    }

    private void onRegisterFailed() {
        confirmButton.setEnabled(true);
    }

    private void onRegisterSuccess() {
        confirmButton.setEnabled(true);

        finish();
    }

    /**
     * Action performed when loginTextView is touched.
     * @param view loginTextView
     */
    public void login(View view) {
        // Finishing this activity we go back to login activity
        finish();
    }
}
