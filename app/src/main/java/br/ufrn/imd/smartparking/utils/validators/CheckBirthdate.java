package br.ufrn.imd.smartparking.utils.validators;

/**
 * Check if a birthdate input is valid.
 *
 * @author Rubem Kalebe
 * @version 10.21.2016
 */

public class CheckBirthdate {

    /**
     * Check if a birthdate input is valid.
     * For now, we just verify the string length.
     * @param date Birthdate entered by the user
     * @return True if date length is greater than 0 and False, otherwise
     */
    public static boolean isValid(String date) {
        return date.length() > 0;
    }

}
