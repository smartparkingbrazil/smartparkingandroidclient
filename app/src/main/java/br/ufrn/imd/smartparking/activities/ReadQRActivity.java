package br.ufrn.imd.smartparking.activities;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.widget.Toast;

import com.google.zxing.Result;

import br.ufrn.imd.smartparking.R;
import br.ufrn.imd.smartparking.app.AppConfig;
import me.dm7.barcodescanner.zxing.ZXingScannerView;

/**
 * This activity is responsible for read a QR code on a parking spot.
 * It returns the code read for the previous activity.
 * We are using ZXing library.
 *
 * @author Rubem Kalebe
 * @version 10.21.2016
 */
public class ReadQRActivity extends AppCompatActivity implements ZXingScannerView.ResultHandler {

    // Object for read QR code
    private ZXingScannerView mScannerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_identify_spot);

        mScannerView = new ZXingScannerView(this); // Programmatically initialize the scanner view
        setContentView(mScannerView); // Set the scanner view as the content view
    }

    @Override
    protected void onResume() {
        super.onResume();

        mScannerView.setResultHandler(this); // Register ourselves as a handler for scan results.
        mScannerView.startCamera(); // Start camera

        // Show instructional message
        Toast toast = Toast.makeText(getApplicationContext(), R.string.scan_qr_msg, Toast.LENGTH_LONG);
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.show();
    }

    @Override
    public void onPause() {
        super.onPause();
        mScannerView.stopCamera(); // Release camera
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    @Override
    public void handleResult(Result result) {
        mScannerView.stopCamera(); // Release camera

        // Return QR read
        Intent returnIntent = new Intent();
        returnIntent.putExtra(AppConfig.QR_CODE, result.getText());
        setResult(Activity.RESULT_OK, returnIntent);
        finish();
    }
}
