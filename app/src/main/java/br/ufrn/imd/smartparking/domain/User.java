package br.ufrn.imd.smartparking.domain;

import java.io.Serializable;

/**
 * This class represents an app user.
 *
 * @author Rubem Kalebe
 * @version 07.03.2017
 */

public class User implements Serializable {

    // User's full name
    private String fullName;

    // User's birthdate
    private String birthdate;

    // User's phone number
    private String phoneNumber;

    // User's email
    private String email;

    // User's Google ID
    private String googleID;

    // User's id
    private String idDocument;


    /**
     * Empty constructor.
     * It initializes the attributes.
     */
    public User() {
        this.fullName = "";
        this.birthdate = "";
        this.phoneNumber = "";
        this.email = "";
        this.googleID = "";
        this.idDocument = "";
    }

    /**
     * It initializes the object with these arguments.
     * @param fullName User's full name
     * @param birthdate User's birthdate
     * @param phoneNumber User's phone number
     * @param email User's email
     * @param googleID User's Google ID
     * @param idDocument User's id
     */
    public User(String fullName, String birthdate, String phoneNumber, String email,
                String googleID, String idDocument) {
        this.fullName = fullName;
        this.birthdate = birthdate;
        this.phoneNumber = phoneNumber;
        this.email = email;
        this.googleID = googleID;
        this.idDocument = idDocument;
    }

    /**
     *
     * @return User full name
     */
    public String getFullName() {
        return fullName;
    }

    /**
     * Changes user full name
     * @param fullName User full name
     */
    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    /**
     *
     * @return User birthdate
     */
    public String getBirthdate() {
        return birthdate;
    }

    /**
     * Changes user birthdate
     * @param birthdate User birthdate
     */
    public void setBirthdate(String birthdate) {
        this.birthdate = birthdate;
    }

    /**
     *
     * @return User phone number
     */
    public String getPhoneNumber() {
        return phoneNumber;
    }

    /**
     * Changes user phone number
     * @param phoneNumber User phone number
     */
    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    /**
     *
     * @return User id
     */
    public String getIdDocument() {
        return idDocument;
    }

    /**
     * Changes user id
     * @param idDocument User id
     */
    public void setIdDocument(String idDocument) {
        this.idDocument = idDocument;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getGoogleID() {
        return googleID;
    }

    public void setGoogleID(String googleID) {
        this.googleID = googleID;
    }

    @Override
    public String toString() {
        return "'" + fullName + '\'';
    }

}
